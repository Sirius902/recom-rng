use recom_lib::gim::CardsUnlocked;
use recom_lib::predictor::Character;

#[derive(Clone, PartialEq)]
pub struct Config {
    pub character: Option<Character>,
    pub cards_unlocked: CardsUnlocked,
}
