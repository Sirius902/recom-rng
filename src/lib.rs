#![recursion_limit = "256"]
#![feature(option_result_contains)]

use yew::prelude::*;

mod config;
use config::Config;

mod screen;
use screen::Screen;

mod components;
use components::{Settings, Field};

pub struct Model {
    link: ComponentLink<Self>,
    screen: Screen,
    config: Config,
}

pub enum Msg {
    ScreenChange(Screen),
    ConfigUpdate(Config),
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Model {
            link,
            screen: Screen::Field,
            config: Config::default(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::ScreenChange(s) => {
                self.screen = s;
                true
            },
            Msg::ConfigUpdate(config) => {
                self.config = config;
                false
            },
        }
    }

    fn view(&self) -> Html {
        let screen_change = |s: Screen| self.link.callback(move |_| Msg::ScreenChange(s));
        let show_settings = !(self.screen == Screen::Settings);
        let show_field = !(self.screen == Screen::Field);

        html! {
            <div class="content">
                <h2>{ "Recom RNG" }</h2>
                // TODO: Remove when finished
                <h3>{ "WIP" }</h3>
                <a id="code-link" href="https://gitlab.com/Sirius902/recom-rng" target="_blank">{ "View Code Here" }</a>
                <div id="screen-select">
                    { Screen::Field.nav_link(self.screen, screen_change) }
                    <span>{ "・" }</span>
                    { Screen::Settings.nav_link(self.screen, screen_change) }
                </div>
                <Settings
                    config=self.config.clone()
                    hidden=show_settings
                    onupdate=self.link.callback(Msg::ConfigUpdate) />
                <Field
                    config=self.config.clone()
                    hidden=show_field />
            </div>
        }
    }
}
