use crate::config::Config;

use yew::prelude::*;
use yew::components::Select;

use strum::IntoEnumIterator;

use recom_lib::card::CardCategory;
use recom_lib::gim::UnlockableCard;
use recom_lib::predictor::Character;

impl Default for Config {
    fn default() -> Self {
        Config {
            character: Some(Character::Sora),
            cards_unlocked: UnlockableCard::all(),
        }
    }
}

pub struct Settings {
    link: ComponentLink<Self>,
    config: Config,
    hidden: bool,
    onupdate: Callback<Config>,
}

pub enum Msg {
    UpdateCharacter(Character),
    ToggleCardUnlocked(UnlockableCard),
    AllCardsUnlocked,
    NoCardsUnlocked,
}

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    #[props(required)]
    pub config: Config,
    #[props(required)]
    pub hidden: bool,
    #[props(required)]
    pub onupdate: Callback<Config>,
}

impl Settings {
    fn card_checkbox(&self, c: UnlockableCard) -> Html {
        html! {
            <div class="checkbox">
                <label>
                    <input
                        type="checkbox"
                        checked=self.config.cards_unlocked.contains(&c)
                        oninput=self.link.callback(move |_| Msg::ToggleCardUnlocked(c)) />
                    { c }
                </label>
            </div>
        }
    }
    
    fn card_table(&self) -> Html {
        let mut cards = UnlockableCard::all()
            .into_iter()
            .collect::<Vec<_>>();
    
        // Alphabetically sort cards because they are in a random order
        // due to iterators.
        cards.sort_by(|c1, c2| c1.to_string().cmp(&c2.to_string()));
    
        let attack = |c: &UnlockableCard| c.category() == CardCategory::AttackCard;
        let magic = |c: &UnlockableCard| c.category() == CardCategory::MagicCard;
        let item = |c: &UnlockableCard| c.category() == CardCategory::ItemCard;
        let summon = |c: &UnlockableCard| c.category() == CardCategory::SummonCard;
    
        html! {
            <table id="card-table">
                <tr>
                    <th>{ "Attack Cards" }</th>
                    <th>{ "Magic Cards" }</th>
                    <th>{ "Item Cards" }</th>
                    <th>{ "Summon Cards" }</th>
                </tr>
                <tr>
                    <td>{ for cards.iter().filter(|c| attack(c)).map(|c| self.card_checkbox(*c)) }</td>
                    <td>{ for cards.iter().filter(|c| magic(c)).map(|c| self.card_checkbox(*c)) }</td>
                    <td>{ for cards.iter().filter(|c| item(c)).map(|c| self.card_checkbox(*c)) }</td>
                    <td>{ for cards.iter().filter(|c| summon(c)).map(|c| self.card_checkbox(*c)) }</td>
                </tr>
            </table>
        }
    }
}

impl Component for Settings {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Settings {
            link,
            config: props.config,
            hidden: props.hidden,
            onupdate: props.onupdate,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        let should_render = match msg {
            Msg::UpdateCharacter(c) => {
                self.config.character = Some(c);
                false
            },
            Msg::ToggleCardUnlocked(c) => {
                if self.config.cards_unlocked.contains(&c) {
                    self.config.cards_unlocked.remove(&c);
                } else {
                    self.config.cards_unlocked.insert(c);
                }
                false
            },
            Msg::AllCardsUnlocked => {
                self.config.cards_unlocked = UnlockableCard::all();
                true
            },
            Msg::NoCardsUnlocked => {
                self.config.cards_unlocked = UnlockableCard::none();
                true
            },
        };

        self.onupdate.emit(self.config.clone());
        should_render
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.config = props.config;
        self.hidden = props.hidden;
        self.onupdate = props.onupdate;
        true
    }

    fn view(&self) -> Html {
        if !self.hidden {
            html! {
                <>
                    <div class="chunk">
                        <b>{ "Character: " }</b>
                        <Select<Character>
                            selected=self.config.character
                            options=Character::iter().collect::<Vec<_>>()
                            onchange=self.link.callback(Msg::UpdateCharacter) />
                    </div>
                    <h3>{ "Cards Unlocked" }</h3>
                    <button onclick=self.link.callback(|_| Msg::AllCardsUnlocked)>{ "All" }</button>
                    <button onclick=self.link.callback(|_| Msg::NoCardsUnlocked)>{ "None" }</button>
                    <br/>
                    { self.card_table() }
                </>
            }
        } else {
            html! {}
        }
    }
}
