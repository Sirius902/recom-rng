use crate::config::Config;

use yew::prelude::*;
use yew::components::Select;

use std::fmt;

use strum_macros::EnumIter;
use strum::IntoEnumIterator;

use recom_lib::card::CardCategory;
use recom_lib::gim::{CardsUnlocked, UnlockableCard};
use recom_lib::predictor::{Predictor, Action, Character};
use recom_lib::types::EntityId;
use recom_lib::world::{Room, World};

#[derive(EnumIter, Copy, Clone, PartialEq)]
pub enum ActionType {
    Roll,
    Break,
}

impl fmt::Display for ActionType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            Self::Roll => "Roll",
            Self::Break => "Break"
        })
    }
}

pub struct RollOptions {
    count: usize,    
}

pub struct BreakOptions {
    object: EntityId,
    hit_count: usize,
}

pub struct Field {
    link: ComponentLink<Self>,
    config: Config,
    hidden: bool,
    actions: Vec<Action>,
    room: Option<Room>,
    world: Option<World>,
    action_type: Option<ActionType>,
    selected_object: Option<EntityId>,
    hit_count: usize,
    roll_count: usize,
}

pub enum Msg {
    WorldUpdate(World),
    RoomUpdate(Room),
    AddAction,
    RemoveAction(usize),
    ClearActions,
    ActionTypeUpdate(ActionType),
    SelectObject(EntityId),
    HitCountUpdate(String),
    RollCountUpdate(String),
}

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    #[props(required)]
    pub config: Config,
    #[props(required)]
    pub hidden: bool,
}

impl Field {
    /// Clears all world and room specific information to prevent strange
    /// behavior. Should be called when changing world and room select.
    fn change_clear(&mut self) {
        self.actions.clear();
        self.selected_object = None;
    }

    fn view_roll_options(&self) -> Html {
        html! {
            <div class="chunk">
                <b>{ "Count: " }</b>
                <input
                    value={ self.roll_count }
                    type="number"
                    min="0"
                    oninput=self.link.callback(|e: InputData| Msg::RollCountUpdate(e.value)) />
            </div>
        }
    }

    fn gimmick_image(id: EntityId) -> Html {
        let path = format!("img/{:X}.jpg", id);
        html! {
            <img
                src={ path }
            ></img>
        }
    }

    // TODO: Finish implementing
    fn view_break_options(&self) -> Html {
        html! {
            <div class="chunk">
                <b>{ "Hit Count: " }</b>
                <input
                    value={ self.hit_count }
                    type="number"
                    min="0"
                    oninput=self.link.callback(|e: InputData| Msg::HitCountUpdate(e.value)) />
                { for self.world.clone().unwrap().gimmicks().into_iter().map(|id| Self::gimmick_image(id)) }
            </div>
        }
    }

    fn view_action_options(&self) -> Html {
        self.action_type.map(|at| {
            match at {
                ActionType::Roll => self.view_roll_options(),
                ActionType::Break => self.view_break_options(),
            }
        }).unwrap_or(html! {})
    }

    fn view_action_builder(&self) -> Html {
        html! {
            <div class="chunk">
                <div class="chunk">
                    <b>{ "World: " }</b>
                    <Select<World>
                        selected=self.world.clone()
                        options=World::iter().collect::<Vec<_>>()
                        onchange=self.link.callback(Msg::WorldUpdate) />
                </div>
                <div class="chunk">
                    <b>{ "Room: " }</b>
                    <Select<Room>
                        selected=self.room.clone()
                        options=Room::iter().collect::<Vec<_>>()
                        onchange=self.link.callback(Msg::RoomUpdate) />
                </div>
                <div>
                    <h3>{ "Action Builder" }</h3>
                    <div class="chunk">
                        <b>{ "Action: " }</b>
                        <Select<ActionType>
                            selected=self.action_type.clone()
                            options=ActionType::iter().collect::<Vec<_>>()
                            onchange=self.link.callback(Msg::ActionTypeUpdate) />
                    </div>
                    { self.view_action_options() }
                    <button onclick=self.link.callback(|_| Msg::AddAction)>{ "Add" }</button>
                </div>
            </div>
        }
    }

    // TODO: Finish
    fn view_table_rows(&self) -> Html {
        let predictor = self.world.and_then(|w|
            self.room.and_then(|r|
                self.config.character.map(|character|
                    Predictor::new(w, r, self.config.cards_unlocked.clone(), character)
                )
            )
        );

        if let Some(mut predictor) = predictor {
            let remove_action = |idx| self.link.callback(move |_| Msg::RemoveAction(idx));
            let action_to_result =
                self.actions.iter().zip(predictor.predict_all(&self.actions));

            html! {
                { for action_to_result.enumerate().map(|(i, (a, r))| {
                    html! {
                        <tr>
                            <td><button onclick=remove_action(i)>{ "Remove" }</button></td>
                            <td></td>
                            <td>{ r.map(|r| r.to_string()).unwrap_or("None".to_owned()) }</td>
                        </tr>
                    }
                }) }
            }
        } else {
            html! {}
        }
    }

    fn view_action_table(&self) -> Html {
        html! {
            <div class="chunk">
                <button onclick=self.link.callback(|_| Msg::ClearActions)>{ "Clear" }</button>
                <table>
                    <tr>
                        <th></th>
                        <th>{ "Action" }</th>
                        <th>{ "Result" }</th>
                    </tr>
                    { self.view_table_rows() }
                </table>
            </div>
        }
    }

    fn roll_options(&self) -> Option<RollOptions> {
        Some(RollOptions { count: self.roll_count })
    }

    fn break_options(&self) -> Option<BreakOptions> {
        self.selected_object.map(|so|
                BreakOptions { object: so, hit_count: self.hit_count }    
        )
    }
}

impl Component for Field {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        let r = Room::UnknownRoom;
        let w = World::TraverseTown;

        Field {
            link,
            config: props.config,
            hidden: props.hidden,
            actions: vec![],
            room: Some(r),
            world: Some(w),
            action_type: Some(ActionType::Roll),
            selected_object: None,
            hit_count: 0,
            roll_count: 0,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::WorldUpdate(world) => {
                self.world = Some(world);
                self.change_clear();
                true
            },
            Msg::RoomUpdate(room) => {
                self.room = Some(room);
                self.change_clear();
                true
            },
            Msg::AddAction => {
                self.action_type.and_then(|at| {
                    match at {
                        ActionType::Roll => {
                            self.roll_options().map(|ro| {
                                self.actions.push(Action::Roll {
                                    count: ro.count
                                });
                                true
                            })
                        },
                        ActionType::Break => {
                            self.break_options().map(|bo| {
                                self.actions.push(Action::Break {
                                    gimmick_id: bo.object,
                                    hit_count: bo.hit_count,
                                });
                                true
                            })
                        },
                    }
                }).unwrap_or(false)
            },
            Msg::ClearActions => {
                self.actions.clear();
                true
            },
            Msg::RemoveAction(idx) => {
                let _ = self.actions.remove(idx);
                true
            },
            Msg::ActionTypeUpdate(action_type) => {
                self.action_type = Some(action_type);
                true
            },
            Msg::SelectObject(id) => {
                self.selected_object = Some(id);
                true
            },
            Msg::HitCountUpdate(count) => {
                if let Ok(count) = count.parse::<usize>() {
                    self.hit_count = count;
                    true
                } else {
                    false
                }
            },
            Msg::RollCountUpdate(count) => {
                if let Ok(count) = count.parse::<usize>() {
                    self.roll_count = count;
                    true
                } else {
                    false
                }
            },
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.config = props.config;
        self.hidden = props.hidden;
        true
    }

    fn view(&self) -> Html {
        if !self.hidden {
            html! {
                <>
                    <h3>{ "Field Settings" }</h3>
                    { self.view_action_builder() }
                    { self.view_action_table() }
                </>
            }
        } else {
            html! {}
        }
    }
}
