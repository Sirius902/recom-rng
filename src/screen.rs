use yew::prelude::*;

#[derive(Copy, Clone, PartialEq)]
pub enum Screen {
    Field,
    Settings,
}

impl Screen {
    pub fn link_name(self) -> &'static str {
        match self {
            Screen::Field => "Field RNG",
            Screen::Settings => "Settings"
        }
    }

    pub fn link_style(self, current: Screen) -> &'static str {
        if self == current {
            "link-like-current"
        } else {
            "link-like"
        }
    }

    pub fn nav_link(self, current: Screen, callback: impl Fn(Screen) -> Callback<ClickEvent>) -> Html {
        html! {
            <span
                class=self.link_style(current)
                onclick=callback(self)>
            { self.link_name() }
            </span>
        }
    }
}
